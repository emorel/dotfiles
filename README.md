# Dotfiles

My system config files and custom scripts.

![screenfeetch](https://i.imgur.com/N2piYs2.png)

## Fonts

- Overpass
- Font Awesome 5

## Packages

Quick description of the packages found in `packages/package.list`, which are quite useful (though not required for a working i3 install).  
Optional / not-so-useful packages can be found in `packages/optionals.list`. Note : some of those aren't present in default repos for apt.

- Rofi  
A lightweight application launcher / switcher.
- Terminator  
A powerful terminal.
- Polybar  
A lightweight bar that shows useful (or not) information.
- Shutter  
A lightweight utility to take screenshot of specific screen region.
- Feh  
A lightweight image viewer. In this case, mostly used to set the wallpaper.
- Compton  
A lightweight X11 compositor.

## .bash_aliases

Defines some useful aliases for commands I use often.

## Installation Tips & Edge Cases

### i3scripts

Copy the `i3scripts` folder into your home directory, the i3 config is set up in a way that it should execute scripts from this folder automatically without further configuration.  
This is currently only used for a workaround for some distributions not properly using the trackpad enable/disable button on laptops.

Optionally, install `libnotify-bin` to display a message when enabling / disabling the trackpad.

### Wallpaper
The i3 config sets `~/Pictures/wallpaper.png` as wallpaper by default. Either add your own wallpaper as this name or change the config to use another image.

This requires `feh` which is in the list of packages.

## Thanks

- **Grafikart** - For taking the efforts to make a video about i3 and to set up his dotfiles repo on [github](https://github.com/grafikart/dotfiles). I used it as a basis for my config :)
