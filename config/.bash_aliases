#!/bin/bash

alias rspec='bundle exec rspec'
alias rails='bundle exec rails'
alias dockerc='docker-compose'
alias rake='bundle exec rake'
