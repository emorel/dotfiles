#!/bin/bash

touchpad_state=$(synclient -l | grep TouchpadOff | cut -d = -f 2)
if [ $touchpad_state -eq 0 ]; then
    synclient TouchpadOff=1
    echo "disabled"
else
    synclient TouchpadOff=0
    echo "enabled"
fi